#!/bin/sh
xrandr --output eDP-1 --mode 1920x1080 --pos 0x1008 --rotate normal --output DP-1-1 --mode 2560x1440 --pos 4480x0 --rotate left --output HDMI-2 --off --output HDMI-1 --off --output DP-1-8 --primary --mode 2560x1440 --pos 1920x648 --rotate normal --output DP-2 --off --output DP-1 --off
