# Path to your oh-my-zsh installation.
export ZSH=/home/johnd/.oh-my-zsh

# Set name of the theme to load.
# Look in ~/.oh-my-zsh/themes/
# Optionally, if you set this to "random", it'll load a random theme each
# time that oh-my-zsh is loaded.
ZSH_THEME="odin"

# Uncomment the following line to use case-sensitive completion.
CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion. Case
# sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
 DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# The optional three formats: "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(git battery kitchen terraform kube-ps1)

# User configuration

export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/home/johnd/source/datasift/oldworld/chef/bin:~/.bin:~/.gem/ruby/2.6.0/bin:/usr/bin/vendor_perl:/home/johnd/.local/bin
# export MANPATH="/usr/local/man:$MANPATH"

source $ZSH/oh-my-zsh.sh

# You may need to manually set your language environment
 export LANG=en_GB.utf8
 export LC_ALL=en_GB.utf8
 export LC_CTYPE=en_GB.utf8

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# ssh
 export SSH_KEY_PATH="~/.ssh/id_rsa"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"

# Dont trust the command hash
zstyle ":completion:*:commands" rehash 1
zstyle ":completion:*:all-files" rehash 1

#if [[ -n ${terminfo[smkx]} ]] && [[ -n ${terminfo[rmkx]} ]]; then
        #function zle-line-init () {echoti smkx}
        #function zle-line-finish () {echoti rmkx}
#
        #zle -N zle-line-init
        #zle -N zle-line-finish
#fi

AWSENV="none"

# User specific aliases and functions
encForAllOps()
{
        gpg --output $1.pgp --encrypt --armour --recipient alex@mediasift.com --recipient andrew.murphy@datasift.com --recipient john.deverteuil@datasift.com --recipient gary.goodger@datasift.com $1
}

encForNetOps()
{
        gpg --output $1.pgp --armour --encrypt --recipient alex@mediasift.com --recipient gary.goodger@datasift.com --recipient john.deverteuil@datasift.com $1
}

encForInfraOps()
{
         gpg --output $1.pgp --encrypt --armour --recipient alex@mediasift.com  --recipient john.deverteuil@datasift.com $1
}

function topTalkers () { echo "bytes\t\t source\t\t\t dest"; cat $1 | awk '{print $9, $1, $3, $5}' |sort -nr | head -10}

function chef_fhais() { export ORGNAME="fhai-stage" && source ~/.oh-my-zsh/themes/robbyrussell-jdv.zsh-theme }
function chef_fhaip() { export ORGNAME="fhai-prod" && source ~/.oh-my-zsh/themes/robbyrussell-jdv.zsh-theme }
function chef_dev() { export ORGNAME="development" && source ~/.oh-my-zsh/themes/robbyrussell-jdv.zsh-theme && export PACKAGECLOUD_GENERAL_V1="bf2c09ccf435a6087f72e3e6362e3f150272a1ac1464605c" }
function chef_stage() { export ORGNAME="staging" && source ~/.oh-my-zsh/themes/robbyrussell-jdv.zsh-theme }
function chef_prod() { export ORGNAME="production" && source ~/.oh-my-zsh/themes/robbyrussell-jdv.zsh-theme }
function chef_none() { unset ORGNAME && source ~/.oh-my-zsh/themes/$ZSH_THEME.zsh-theme && unset PACKAGECLOUD_GENERAL_V1 }

function os_admin() { source ~/.osadminrc && source ~/.oh-my-zsh/themes/robbyrussell-jdv-os.zsh-theme }
function os_johnd() { source ~/.osjohndrc && source ~/.oh-my-zsh/themes/robbyrussell-jdv-os.zsh-theme }
function os_ms() { source ~/.osmicrosoftrc && source ~/.oh-my-zsh/themes/robbyrussell-jdv-os.zsh-theme }
function os_none() { source ~/.osnonerc && source ~/.oh-my-zsh/themes/$ZSH_THEME.zsh-theme }

function aws_fhaip() { export AWSENV="fhaip" && source ~/.oh-my-zsh/themes/robbyrussell-jdv-aws.zsh-theme }
function aws_fhaid() { export AWSENV="fhaid" && source ~/.oh-my-zsh/themes/robbyrussell-jdv-aws.zsh-theme }
function aws_none() { export AWSENV="none" && source ~/.oh-my-zsh/themes/$ZSH_THEME.zsh-theme }
#function kon() { source ~/.oh-my-zsh/themes/$ZSH_THEME.zsh-theme && PROMPT=$PROMPT'$(kube_ps1):' }
#function koff() { source ~/.oh-my-zsh/themes/$ZSH_THEME.zsh-theme }

#function aws() {aws --profile $AWSENV $@ }

function rec_tmux() {for i in $(tmux ls | grep -v atta | cut -f 1 -d ':'); do lilyterm -s -e /usr/bin/tmux attach -t $i &!; done}
function sony() {bluetoothctl disconnect 38:18:4C:10:B3:B5 && bluetoothctl connect 38:18:4C:10:B3:B5}


alias gpgallops=encForAllOps
alias gpgnetops=encForNetOps
alias gpgnetops=encForInfraOps
alias gpg=gpg2
alias cdc='cd /home/johnd/source/datasift/nwo/cookbooks'
alias cdm='cd /home/johnd/source/meltwater/'
alias kr='ssh-keygen -R'
alias pjson="python -c 'import fileinput, json; print(json.dumps(json.loads(\"\".join(fileinput.input())),sort_keys=True, indent=4))'"
alias awsp='aws --profile fhaip'
alias awsd='aws --profile fhaid'


alias diff='diff -W $(( $(tput cols) - 2 ))'

autoload -U compinit && compinit

GPG_TTY=$(tty)
export GPG_TTY

unset SSH_AGENT_PID
if [ "${gnupg_SSH_AUTH_SOCK_by:-0}" -ne $$ ]; then
  USER_ID=$(id -u $(whoami))
  export SSH_AUTH_SOCK="/run/user/$USER_ID/gnupg/S.gpg-agent.ssh"
fi

