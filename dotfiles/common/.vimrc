" size of a hard tabstop
set tabstop=2

" always uses spaces instead of tab characters
set expandtab

" size of an "indent"
set shiftwidth=2

" highlight overlength.
" highlight OverLength ctermbg=red ctermfg=white guibg=#592929
" match OverLength /\%81v.\+/

" highlight TrailingWhite ctermbg=blue ctermfg=white guibg=#592929
" match TrailingWhite /\s\+$/

autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o
